import openpyxl
import sqlite3

#Create a new workbook and worksheet
wb = openpyxl.Workbook()
ws = wb.worksheets[0]
ws.title = "Temperature by city"

#Get all city entries from China
sqlCommand = """
SELECT DATE, AVERAGE_TEMP, CITY FROM GLOBAL_LAND_TEMPERATURES_BY_MAJOR_CITY
WHERE COUNTRY = "China";
"""
conn = sqlite3.connect("database.db")
cursor = conn.cursor()
cursor.execute(sqlCommand)
data = cursor.fetchall()

#Get a list of all cities to help with data analysis
cities = list()
for row in data:
    if not row[2] in cities:
        cities.append(row[2])

#Calculates the yearly mean temperature of each city in China and writes the data to the workbook
#For every city
#   Go through every row that matches that city
#       Add each temperature entry to a dictionary(year => temp)
#   Go through every year in dictionary
#       Calculate the yearly mean and write it to the workbook
y = 0
for city in cities:
    table = dict()
    for row in data:
        if row[2] == city:
            year = row[0][:4]
            if not year in table:
                table[year] = list()
            table[year].append(row[1])
    for year in table:
        total = 0
        for value in table[year]:
            if not value is None:
                total += value
        yearly_mean = total / len(table[year])
        ws.cell(row=y+1, column=1).value = city
        ws.cell(row=y+1, column=2).value = year
        ws.cell(row=y+1, column=3).value = yearly_mean
        y += 1

#Save the work
wb.save("World Temperature.xlsx")