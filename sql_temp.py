import sqlite3
import numpy

#Connect to database and create table
#If table already exists, drop it and create from scratch
conn = sqlite3.connect("database.db")
cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS SOUTHERN_CITIES;")
sqlCommand = """
CREATE TABLE SOUTHERN_CITIES
(
    CITY TEXT PRIMARY KEY,
    COUNTRY TEXT NOT NULL,
    LATITUDE TEXT NOT NULL,
    LONGITUDE TEXT NOT NULL
);
"""
cursor.execute(sqlCommand)

#Get all countries in the southern hemisphere in alphabetical order
sqlCommand = """
SELECT CITY, COUNTRY, LATITUDE, LONGITUDE FROM GLOBAL_LAND_TEMPERATURES_BY_MAJOR_CITY
WHERE LATITUDE LIKE '%S'
ORDER BY COUNTRY ASC;
"""
cursor.execute(sqlCommand)
#Insert that data into the new Southern_Cities table
results = cursor.fetchall()
table = list()
for result in results:
    if not result[0] in table:
        table.append(result[0])
        sqlCommand = """
        INSERT INTO SOUTHERN_CITIES (CITY, COUNTRY, LATITUDE, LONGITUDE)
        VALUES(?, ?, ?, ?);
        """
        cursor.execute(sqlCommand, (result[0], result[1], result[2], result[3]))

#Print list of cities to console
print(table)

#Get all entries for Queensland in 2010
sqlCommand = """
SELECT AVERAGE_TEMP FROM GLOBAL_LAND_TEMPERATURES_BY_STATE
WHERE DATE LIKE '2010%'
AND STATE = 'Queensland';
"""
cursor.execute(sqlCommand)
#Determine the min, max and mean using a NumPy array
qld_temps = cursor.fetchall()
qld_temps_nparray = numpy.array(qld_temps)
print(qld_temps_nparray.max(), qld_temps_nparray.min(), qld_temps_nparray.mean())
conn.commit()
conn.close()