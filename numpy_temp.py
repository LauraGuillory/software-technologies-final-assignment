import openpyxl
import sqlite3
import matplotlib.pyplot as plt

#Open the workbook and create a new worksheet
#If the sheet already exists, delete it and start over
wb = openpyxl.load_workbook("World Temperature.xlsx")
if "Comparison" in wb.sheetnames:
    wb.remove_sheet(wb.get_sheet_by_name("Comparison"))
ws = wb.create_sheet("Comparison")
ws.cell(row=1, column=1).value = "State"
ws.cell(row=1, column=2).value = "Year"
ws.cell(row=1, column=3).value = "Average Temperature Diff"
#Get every state entry from Australia
sqlCommand = """
SELECT DATE, AVERAGE_TEMP, STATE FROM GLOBAL_LAND_TEMPERATURES_BY_STATE
WHERE COUNTRY = "Australia";
"""
conn = sqlite3.connect("database.db")
cursor = conn.cursor()
cursor.execute(sqlCommand)
data = cursor.fetchall()

#Calculate thet mean temperature of Australian states for each year
states = list()
for row in data:
    if not row[2] in states:
        states.append(row[2])
state_yearly_means = list()
for state in states:
    table = dict()
    for row in data:
        if row[2] == state:
            year = row[0][:4]
            if not year in table:
                table[year] = list()
            if row[1]:
                table[year].append(row[1])
    for year in table:
        total = 0
        for value in table[year]:
            if not value is None:
                total += value
        if total > 0:
            yearly_mean = total / len(table[year])
            state_yearly_means.append((state, year, yearly_mean))

#Get every country entry from Australia
sqlCommand = """
SELECT DATE, AVERAGE_TEMP FROM GLOBAL_LAND_TEMPERATURES_BY_COUNTRY
WHERE COUNTRY = "Australia";
"""
cursor.execute(sqlCommand)
data = cursor.fetchall()
#Calculate the mean temperature of Australia for each year
table = dict()
for row in data:
    year = row[0][:4]
    if not year in table:
        table[year] = list()
    if row[1]:
        table[year].append(row[1])
country_yearly_means = dict()
for year in table:
    total = 0
    for value in table[year]:
        if not value is None:
            total += value
    if total > 0:
        yearly_mean = total / len(table[year])
        country_yearly_means[year] = yearly_mean

#Filter out state entries that don't have a corresponding year in the country entries
state_yearly_means = list(filter(lambda x: not x[1] == 0 and x[1] in country_yearly_means, state_yearly_means))

#Write data to workbook and organise our two lists to be more MatplotLib friendly
graph_data_x = list()
graph_data_y = list()
row = 2
for state in range(0, len(states)):
    graph_data_x.append(list())
    graph_data_y.append(list())
    for entry in state_yearly_means:
        if states[state] == entry[0]:
            graph_data_x[state].append(int(entry[1]))
            graph_data_y[state].append(entry[2] - country_yearly_means[entry[1]])
            ws.cell(row=row, column=1).value = entry[0]
            ws.cell(row=row, column=2).value = int(entry[1])
            ws.cell(row=row, column=3).value = entry[2] - country_yearly_means[entry[1]]
            row += 1

wb.save("World Temperature.xlsx")

#Plot the data
plt.plot(graph_data_x[0], graph_data_y[0], graph_data_x[1], graph_data_y[1],
         graph_data_x[2], graph_data_y[2], graph_data_x[3], graph_data_y[3],
         graph_data_x[4], graph_data_y[4], graph_data_x[5], graph_data_y[5],
         graph_data_x[6], graph_data_y[6], graph_data_x[7], graph_data_y[7])
plt.title("Difference between state average and national average temperature")
plt.xlabel("Year")
plt.ylabel("Difference in centigrade")
plt.show()
