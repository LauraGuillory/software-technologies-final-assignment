import openpyxl
import sqlite3
import datetime

#Loading data from excel
ws_byCountry = openpyxl.load_workbook("GlobalLandTemperaturesByCountry.xlsx").worksheets[0]
ws_byMajorCity = openpyxl.load_workbook("GlobalLandTemperaturesByMajorCity.xlsx").worksheets[0]
ws_byState = openpyxl.load_workbook("GlobalLandTemperaturesByState.xlsx").worksheets[0]
#Connect to database and create tables
#If the tables already exist, drop them and start from scratch
conn = sqlite3.connect("database.db")
cursor = conn.cursor()
cursor.execute("DROP TABLE IF EXISTS GLOBAL_LAND_TEMPERATURES_BY_COUNTRY;")
sqlCommand = """
CREATE TABLE GLOBAL_LAND_TEMPERATURES_BY_COUNTRY
(
   DATE TEXT,
   AVERAGE_TEMP REAL,
   AVERAGE_TEMP_UNCERTAINTY REAL,
   COUNTRY TEXT NOT NULL,
   PRIMARY KEY (DATE, COUNTRY)
);
"""
cursor.execute(sqlCommand)
cursor.execute("DROP TABLE IF EXISTS GLOBAL_LAND_TEMPERATURES_BY_MAJOR_CITY;")
sqlCommand = """
CREATE TABLE GLOBAL_LAND_TEMPERATURES_BY_MAJOR_CITY
(
    DATE TEXT,
    AVERAGE_TEMP REAL,
    AVERAGE_TEMP_UNCERTAINTY REAL,
    CITY TEXT NOT NULL,
    COUNTRY TEXT NOT NULL,
    LATITUDE TEXT NOT NULL,
    LONGITUDE TEXT NOT NULL,
    PRIMARY KEY (DATE, CITY)
);
"""
cursor.execute(sqlCommand)
cursor.execute("DROP TABLE IF EXISTS GLOBAL_LAND_TEMPERATURES_BY_STATE;")
sqlCommand = """
CREATE TABLE GLOBAL_LAND_TEMPERATURES_BY_STATE
(
    DATE TEXT,
    AVERAGE_TEMP REAL,
    AVERAGE_TEMP_UNCERTAINTY REAL,
    STATE TEXT NOT NULL,
    COUNTRY TEXT NOT NULL,
    PRIMARY KEY (DATE, STATE)
);
"""
cursor.execute(sqlCommand)

#Import data from Country workbook
length = ws_byCountry.max_row
for i in range(1, length):
    date = ws_byCountry.cell(row=i+1, column=1).value
    if isinstance(date, datetime.datetime):
        date = date.strftime("%Y-%m-%d")
    averageTemp = ws_byCountry.cell(row=i+1, column=2).value
    averageTempUncertainty = ws_byCountry.cell(row=i+1, column=3).value
    country = ws_byCountry.cell(row=i+1, column=4).value
    sqlCommand = """
    INSERT INTO GLOBAL_LAND_TEMPERATURES_BY_COUNTRY (DATE, AVERAGE_TEMP, AVERAGE_TEMP_UNCERTAINTY, COUNTRY)
    VALUES(?, ?, ?, ?);
    """
    cursor.execute(sqlCommand, (date, averageTemp, averageTempUncertainty, country))

#Import data from City workbook
length = ws_byMajorCity.max_row
for i in range(1, length):
    date = ws_byMajorCity.cell(row=i+1, column=1).value
    if isinstance(date, datetime.datetime):
        date = date.strftime("%Y-%m-%d")
    averageTemp = ws_byMajorCity.cell(row=i+1, column=2).value
    averageTempUncertainty = ws_byMajorCity.cell(row=i+1, column=3).value
    city = ws_byMajorCity.cell(row=i+1, column=4).value
    country = ws_byMajorCity.cell(row=i+1, column=5).value
    latitude = ws_byMajorCity.cell(row=i+1, column=6).value
    longitude = ws_byMajorCity.cell(row=i+1, column=7).value
    sqlCommand = """
    INSERT INTO GLOBAL_LAND_TEMPERATURES_BY_MAJOR_CITY 
    (DATE, AVERAGE_TEMP, AVERAGE_TEMP_UNCERTAINTY, CITY, COUNTRY, LATITUDE, LONGITUDE)
    VALUES(?, ?, ?, ?, ?, ?, ?);
    """
    cursor.execute(sqlCommand, (date, averageTemp, averageTempUncertainty, city, country, latitude, longitude))

#Import data from State workbook
length = ws_byState.max_row
for i in range(1, length):
    date = ws_byState.cell(row=i+1, column=1).value
    if isinstance(date, datetime.datetime):
        date = date.strftime("%Y-%m-%d")
    averageTemp = ws_byState.cell(row=i+1, column=2).value
    averageTempUncertainty = ws_byState.cell(row=i+1, column=3).value
    state = ws_byState.cell(row=i+1, column=4).value
    country = ws_byState.cell(row=i+1, column=5).value
    sqlCommand = """
    INSERT INTO GLOBAL_LAND_TEMPERATURES_BY_STATE (DATE, AVERAGE_TEMP, AVERAGE_TEMP_UNCERTAINTY, STATE, COUNTRY)
    VALUES(?, ?, ?, ?, ?);
    """
    cursor.execute(sqlCommand, (date, averageTemp, averageTempUncertainty, state, country))
conn.commit()
conn.close()